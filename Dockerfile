FROM node:12.16.0-alpine3.10

ARG ENV=production

RUN mkdir /app
WORKDIR /app

ENV NODE_ENV $ENV

CMD npm run $NODE_ENV

COPY package* ./
RUN npm ci

COPY . .

ENV NODE_OPTIONS                        '-r elastic-apm-node/start'
# https://www.elastic.co/guide/en/apm/agent/nodejs/current/configuration.html
ENV ELASTIC_APM_SERVER_URL              http://apm-server.logging:8200
ENV ELASTIC_APM_ACTIVE                  false

EXPOSE 3000
