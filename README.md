# Owerview

### Example: Node.js with using Gitlab's AutoDevOps helm chart

Simple example based on Node.js demonstrating how to use Gitlab's AutoDevOps helm chart.

#### Init

```bash
helm repo add gitlab https://charts.gitlab.io/
helm repo update

skaffold dev --tail
```

Output:
```bash
➜  skaffold-getting-started git:(master) skaffold dev --tail
Listing files to watch...
 - registry.gitlab.com/lazyorangejs/lab/skaffold-getting-started
Generating tags...
 - registry.gitlab.com/lazyorangejs/lab/skaffold-getting-started -> registry.gitlab.com/lazyorangejs/lab/skaffold-getting-started:2020-06-11_05-28-20.043_UTC
Checking cache...
 - registry.gitlab.com/lazyorangejs/lab/skaffold-getting-started: Found. Tagging
Tags used in deployment:
 - registry.gitlab.com/lazyorangejs/lab/skaffold-getting-started -> registry.gitlab.com/lazyorangejs/lab/skaffold-getting-started:2020-06-11_05-28-20.043_UTC@sha256:3d584b856cee6a2aae2e7c17abb7af666ba0951769f61ba503a62073159e1522
Starting deploy...
Helm release skaffold-gitlab-auto-devops not installed. Installing...
coalesce.go:195: warning: destination for requests is a table. Ignoring non-table value <nil>
coalesce.go:195: warning: destination for requests is a table. Ignoring non-table value <nil>
NAME: skaffold-gitlab-auto-devops
LAST DEPLOYED: Thu Jun 11 05:28:29 2020
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
Application was deployed reusing the service at

    http://my.host.com/

It will share a load balancer with the previous release (or be unavailable if
no service was previously deployed).
Waiting for deployments to stabilize...
Deployments stabilized in 51.5352ms
Watching for changes...
[skaffold-gitlab-auto-devops-7fbd6f8ff-qll75 auto-deploy-app] 
[skaffold-gitlab-auto-devops-7fbd6f8ff-qll75 auto-deploy-app] > backend@1.0.0 production /app
[skaffold-gitlab-auto-devops-7fbd6f8ff-qll75 auto-deploy-app] > node src/index.js
[skaffold-gitlab-auto-devops-7fbd6f8ff-qll75 auto-deploy-app] 
[skaffold-gitlab-auto-devops-7fbd6f8ff-qll75 auto-deploy-app] Example app listening on port 3000!
[skaffold-gitlab-auto-devops-7fbd6f8ff-qll75 auto-deploy-app] {"level":20,"time":1591853320006,"pid":17,"hostname":"skaffold-gitlab-auto-devops-7fbd6f8ff-qll75","msg":"The server uses approximately 40 MB"}
[skaffold-gitlab-auto-devops-7fbd6f8ff-qll75 auto-deploy-app] {"level":20,"time":1591853325007,"pid":17,"hostname":"skaffold-gitlab-auto-devops-7fbd6f8ff-qll75","msg":"The server uses approximately 40 MB"}
[skaffold-gitlab-auto-devops-7fbd6f8ff-qll75 auto-deploy-app] {"level":20,"time":1591853330011,"pid":17,"hostname":"skaffold-gitlab-auto-devops-7fbd6f8ff-qll75","msg":"The server uses approximately 40 MB"}
```

#### Helm v3
```bash
helm repo add gitlab https://charts.gitlab.io/
helm repo update
#
helm upgrade --install -f .gitlab/auto-deploy-values.yaml getting-started-skaffold gitlab/auto-deploy-app
```