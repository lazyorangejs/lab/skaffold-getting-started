const express = require('express')
const Prometheus = require('prom-client')
const pino = require('pino')
const { echo } = require('./utils')

const logger = pino({ level: 'debug' })
const app = express()
const port = process.env.PORT || 3000

Prometheus.collectDefaultMetrics()

const checkoutsTotal = new Prometheus.Counter({
  name: 'checkouts_total',
  help: 'Total number of checkouts',
  labelNames: ['payment_method']
})

const httpRequestDurationMicroseconds = new Prometheus.Histogram({
  name: 'http_request_duration_ms',
  help: 'Duration of HTTP requests in ms',
  labelNames: ['method', 'route', 'code'],
  buckets: [0.10, 5, 15, 50, 100, 200, 300, 400, 500]  // buckets for response time from 0.1ms to 500ms
})

// Runs before each requests
app.use((req, res, next) => {
  res.locals.startEpoch = Date.now()
  next()
})

app.get('/metrics', (req, res) => {
  res.set('Content-Type', Prometheus.register.contentType)
  res.end(Prometheus.register.metrics())
})

app.get('/checkout', (_, res, next) => {
  const paymentMethod = Math.round(Math.random()) === 0 ? 'stripe' : 'paypal'

  checkoutsTotal.inc({
    payment_method: paymentMethod
  })

  res.json({ status: 'ok' })

  next()
})

app.get('/', (_, res) => res.send(echo('Hello World!')))
app.get('/version', (_, res) => res.json(process.env.npm_package_version))
app.get('/:username', (req, res) => {
  res.send(echo(`Hello, ${req.params.username}!`))
})

// Runs after each requests
app.use((req, res, next) => {
  const responseTimeInMs = Date.now() - res.locals.startEpoch

  httpRequestDurationMicroseconds
    .labels(req.method, req.route.path, res.statusCode)
    .observe(responseTimeInMs)

  next()
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))

setInterval(() => {
  logger.debug(
    `The server uses approximately ${Math.round(
      process.memoryUsage().rss / (1024 * 1024)
    )} MB`
  )
}, 5_000)
